﻿'use strict';

angular.module('page.contact')
    .config(function ($routeProvider) {

        $routeProvider
            .when('/contact', {
                templateUrl: _globals.pagesPath + 'contact/page.contact.html?v=' + _globals.appVersion,
                resolve: {
                    load: ['$q', function ($q) {
                        var def = $q.defer();

                        // Dependency injection components required for page to work
                        require([
                            'Scripts/app/components/headline/directive.headline'
                        ],
                        function () {
                            // Dependency injection the page controller
                            require([_globals.pagesPath + 'contact/controller.contact'], function () {
                                def.resolve();
                            });
                        });

                        return def.promise;
                    }]
                }
        });
});