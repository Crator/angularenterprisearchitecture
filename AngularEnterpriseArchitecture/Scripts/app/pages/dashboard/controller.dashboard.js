﻿app.lazy.controller('DashboardController', ['$scope', function ($scope) {

    $scope.name = 'Overview';

    $scope.eventAll = function () {
        $scope.$broadcast('onSetTitle', { title: 'Event all' });
    };

    $scope.eventById = function (id) {
        $scope.$broadcast('onSetTitle', { id: id, title: 'Event by id: ' + id });
    };
}]);