﻿'use strict';

angular.module('page.dashboard')
    .config(function ($routeProvider) {

        $routeProvider
            .when('/dashboard', {
                templateUrl: _globals.pagesPath + 'dashboard/page.dashboard.html?v=' + _globals.appVersion,
                resolve: {
                    load: ['$q', function ($q) {
                        var def = $q.defer();

                        // Dependency inject modules required for page to work
                        require([
                            'Scripts/app/services/service.data',
                            'Scripts/app/components/headline/directive.headline',
                            'Scripts/app/components/statusFeed/directive.statusFeed'
                        ],
                        function () {
                            // Dependency inject the page controller
                            require([_globals.pagesPath + 'dashboard/controller.dashboard'], function () {
                                def.resolve();
                            });
                        });

                        return def.promise;
                    }]
                }
            });
});