﻿app.lazy.directive('aeaHeadline', [function () {
    return {
        scope: {
            headline: '@aeaHeadline',
            eventId: '@'
        },
        restrict: 'AE',
        replace: true,
        templateUrl: '/Scripts/app/components/headline/partial.headline.html?v=' + _globals.appVersion,
        link: function ($scope, $element, $attr) {

            // Listeners
            $scope.$on('onSetTitle', function (e, data) {
                if (!data.id) {
                    $scope.headline = data.title;
                }
                else if (data.id === $scope.eventId) {
                    $scope.headline = data.title;
                }
            });
        }
    }
}]);