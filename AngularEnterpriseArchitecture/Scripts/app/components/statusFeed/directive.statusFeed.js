﻿app.lazy.directive('aeaStatusFeed', ['DataService', function (DataService) {
    return {
        scope: {},
        restrict: 'AE',
        replace: true,
        templateUrl: '/Scripts/app/components/statusFeed/partial.statusFeed.html?v=' + _globals.appVersion,
        link: function ($scope, $element, $attr) {

            // Scope variables
            $scope.feed = [
                { value: 'Entry 1' },
                { value: 'Entry 2' },
                { value: 'Entry 3' },
                { value: 'Entry 4' }
            ];

            // Init
            DataService.getDataById(1);
        }
    }
}])