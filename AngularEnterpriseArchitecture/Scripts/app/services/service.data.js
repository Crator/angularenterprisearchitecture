﻿define('Scripts/app/services/service.data', [], function () {
    app.lazy.service('DataService', ['$q', 'CacheFactory', function ($q, CacheFactory) {

        /*
            Internal logic
        ----------------------------------- */
        if (!CacheFactory.get('dataCache')) {
            CacheFactory.createCache('dataCache', {
                maxAge: 3600000
            });
        }

        var _dataCache = CacheFactory.get('dataCache');

        /*
            Service methods
        ----------------------------------- */
        this.getDataById = function (id) {
            var def = $q.defer();

            if (_dataCache.get(id)) {
                console.log("# Return cached data: ", _dataCache.get(id));

                def.resolve(_dataCache.get(id));
            } else {
                console.log("# Get data from server");

                // Get data from backend service via $http
                // ...

                // Put data from backend into cache
                _dataCache.put(id, { some: 'value' });

                def.resolve(_dataCache.get(id));
            }

            return def.promise;
        }
    }]);
});