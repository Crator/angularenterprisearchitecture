﻿'use strict';

var _globals = {
    basePath: 'Scripts/app/',
    pagesPath: 'Scripts/app/pages/',
    appVersion: '2017.1' // for cache busting
};

/*
    Configure requireJs
----------------------------------------*/
require.config({
    urlArgs: 'v=' + _globals.appVersion,
    waitSeconds: 0
});

/*
    Configure angularJs
----------------------------------------*/
angular.module('page.dashboard', []);
angular.module('page.contact', []);

var app = angular.module('enterpriseApp', [
    'ngRoute',
    'angular-cache',

    'page.dashboard',
    'page.contact'
]);

app.config(function ($controllerProvider, $compileProvider, $filterProvider, $provide, $routeProvider, $locationProvider, CacheFactoryProvider) {

    $locationProvider.html5Mode(true);

    app.lazy = {
        controller: $controllerProvider.register,
        directive: $compileProvider.directive,
        filter: $filterProvider.register,
        factory: $provide.factory,
        service: $provide.service,
        //animation: $animationProvider.register
    };

    $routeProvider
        .otherwise({
            redirectTo: '/',
            templateUrl: _globals.pagesPath + 'home/page.home.html?v=' + _globals.appVersion
        });
});